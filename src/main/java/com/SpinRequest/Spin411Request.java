package com.SpinRequest;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Spin411Request extends SpinRequest {

	private int freeSpinIndex;
	private int freeSpins;

}
