package com.SpinRequest;

import lombok.Data;

@Data
public class SpinRequest {
	private String serialNo;
	private String sessionId;
	private String token;
	private String gameCode;
	private double totalBet;

}
