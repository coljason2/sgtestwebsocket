package com.SpinRequest;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class Spin401Request extends SpinRequest {
	private double unit;
	private int line;
	private int credit;
	private double domination;
	private String retainResult;
	private int freeSpin;

}
