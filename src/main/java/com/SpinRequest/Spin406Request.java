package com.SpinRequest;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Spin406Request extends SpinRequest {
	private double domination;
	private int bankerCardId;
	private int gambleId;
}
