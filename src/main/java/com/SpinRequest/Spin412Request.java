package com.SpinRequest;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class Spin412Request extends SpinRequest {
	private double unit;
	private int line;
	private int credit;
	private Double domination;
	private int freeSpin;


}
