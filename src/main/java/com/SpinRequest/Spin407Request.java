package com.SpinRequest;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Spin407Request {
	private String serialNo;
	private String sessionId;
	private String token;
	private String gameCode;
}
