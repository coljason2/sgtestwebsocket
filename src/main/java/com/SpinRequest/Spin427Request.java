package com.SpinRequest;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Spin427Request extends SpinRequest {
	private double domination;
	private int[] locBet;
	private int index;
}
