package com.frame.Menultem;

import java.awt.event.MouseEvent;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserMenuItem extends GenerateMenultem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int users;

	public UserMenuItem(String value) {
		super();
		setText("User: " + value);
		users = Integer.parseInt(value);
	}

	public void mousePressed(MouseEvent e) {
		// log.info("mousePressed " + users);
		PropertiesUtil.setGameUser(users);
		PropertiesUtil.getCommanFrame().setGameUsers(users);
		PropertiesUtil.getTextCmd().getTextA().setText("Set Test Users : " + users + "\n");
		PropertiesUtil.SaveConfig();
	}

}
