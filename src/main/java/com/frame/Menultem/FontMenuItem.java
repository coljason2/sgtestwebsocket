package com.frame.Menultem;

import java.awt.Font;
import java.awt.event.MouseEvent;



public class FontMenuItem extends GenerateMenultem {

	private static final long serialVersionUID = 1L;
	private String name;

	public FontMenuItem(String string) {
		super();
		setText(string);
		name = string;
	}


	public void mousePressed(MouseEvent e) {
		textCmd.textFont = new Font(name, textCmd.textFont.getStyle(), textCmd.textFont.getSize());
		textCmd.textA.setFont(textCmd.textFont);
		saveConfigStatus();
	}

}
