package com.frame.Menultem;

import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class HelpMenuItem extends GenerateMenultem {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
	private static final String creater = "Create by JunChiChen";
	private static final String Version = "Version 1.2.0";
	private static final String Date = "2018-01-31";
	private static final String Content = "This application is for SpadeGaming test, \n if you have any problem please contact creater.";

	public HelpMenuItem(String string) {
		super();
		setText(string);
	}


	public void mousePressed(MouseEvent e) {
		String msg = creater + "\n" + Date + "\n" + Version + "\n" + Content;
		JFrame jf = new JFrame();
		jf.setAlwaysOnTop(true);
		jf.setFocusable(true);
		JOptionPane.showMessageDialog(jf, msg, "About", JOptionPane.INFORMATION_MESSAGE, null);

	}

}
