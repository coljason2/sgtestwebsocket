package com.frame.Menultem;

import java.awt.Cursor;
import java.awt.event.MouseEvent;

import com.frame.textArea;
import com.frame.PropertiesUtil;

public abstract class GenerateMenultem extends javax.swing.JMenuItem implements java.awt.event.MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static textArea textCmd;
	protected static PropertiesUtil PropertiesUtil;

	public GenerateMenultem() {
		super();
		this.addMouseListener(this);
	}

	public void mouseEntered(MouseEvent e) {
		setCursor(new Cursor(Cursor.HAND_CURSOR));
	}

	public void mouseExited(MouseEvent e) {
		setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseClicked(MouseEvent e) {

	}

	public void saveConfigStatus() {
		PropertiesUtil.setTextFont(textCmd.textFont);
		PropertiesUtil.setTextCmd(textCmd);
		PropertiesUtil.SaveConfig();

	}

	public static void setPropertiesUtil(PropertiesUtil propertiesUtil) {
		PropertiesUtil = propertiesUtil;
		textCmd = PropertiesUtil.getTextCmd();
	}

}
