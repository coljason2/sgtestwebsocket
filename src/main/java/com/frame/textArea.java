package com.frame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

public class textArea extends JPanel {

	private static final long serialVersionUID = 1L;
	public JTextArea textA = new JTextArea();
	private JScrollPane areaScrollPane = new JScrollPane(textA);
	public Font textFont = new PropertiesUtil().setInitialeTextFont();

	public textArea() {
		textA.setFont(textFont);
		textA.setForeground(Color.WHITE);
		textA.setBackground(Color.BLACK);
		textA.setEditable(false);
		setLayout(new BorderLayout()); // !! added
		// always show last line
		DefaultCaret caret = (DefaultCaret) textA.getCaret(); //
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		add(areaScrollPane, BorderLayout.CENTER);
	}

	public JTextArea getTextA() {
		return textA;
	}

}
