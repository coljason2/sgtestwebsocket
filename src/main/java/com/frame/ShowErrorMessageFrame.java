package com.frame;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JOptionPane;

public class ShowErrorMessageFrame {
	StringWriter sw = new StringWriter();
	PrintWriter pw = new PrintWriter(sw);

	public ShowErrorMessageFrame(Exception e) {
		e.printStackTrace(pw);
		JOptionPane.showMessageDialog(null, sw.toString(), "Error", JOptionPane.ERROR_MESSAGE, null);
	}
}
