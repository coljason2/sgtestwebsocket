package com.frame;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.JTextArea;

import com.game.gameType;
import com.websocketClient.websocketClientFrame;

public class SpinGame implements Runnable {
	private List<websocketClientFrame> gameThreads = new ArrayList<websocketClientFrame>();
	private websocketClientFrame websocketClient;
	private int gameUsers;
	private String url;
	private gameType gameType;
	private JTextArea textA;
	private boolean bonus;
	private Set<String> IsGameBer;

	public SpinGame(int gameUsers, String url, gameType gameType, JTextArea textA, boolean bonus,
			Set<String> IsGameBale) {
		this.gameUsers = gameUsers;
		this.url = url;
		this.gameType = gameType;
		this.textA = textA;
		this.bonus = bonus;
		this.IsGameBer = IsGameBale;
	}

	public void run() {
		for (int i = 0; i < gameUsers; i++) {
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			websocketClient = new websocketClientFrame(url, gameType);
			websocketClient.setArea(textA);
			websocketClient.setNoBonus(bonus);
			websocketClient.setIsGameBer(IsGameBer);
			gameThreads.add(websocketClient);
			new Thread(websocketClient).start();
		}
	}

	public void destory() {
		for (websocketClientFrame g : gameThreads) {
			g.destory();
		}
	}

}
