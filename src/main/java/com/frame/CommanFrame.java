package com.frame;

import java.awt.BorderLayout;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JToolBar;
import com.frame.Menultem.FontMenuItem;
import com.frame.Menultem.HelpMenuItem;
import com.frame.Menultem.MenuScroller;
import com.frame.Menultem.SizeMenuItem;
import com.frame.Menultem.UserMenuItem;
import com.game.GameListFactory;
import com.game.gameType;
import lombok.Getter;
import lombok.Setter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

public class CommanFrame {
	private JFrame LocalService;
	private textArea textCmd = new textArea();
	protected static PropertiesUtil PropertiesUtil = null;
	private static ServerSocket srvSocket = null;
	// Button
	private JButton btnStart = new JButton("Start");
	private final JButton btnStop = new JButton("Stop");
	private final JButton btnClear = new JButton("Clear");
	// JMenu
	private JMenuBar menuBar = new JMenuBar();
	private JMenu mnSetting = new JMenu("Setting");
	private JMenu mnWindows = new JMenu("Windows");
	private JMenu mnHelp = new JMenu("Help");
	private JMenu mmtmUsers = new JMenu("Game Users");
	private JMenu mmtmFont = new JMenu("Font Setting");
	private JMenu mmtmFontSize = new JMenu("Font Size");
	private HelpMenuItem mmtmAbout = new HelpMenuItem("About");
	private MenuScroller fontScrollPane = new MenuScroller(mmtmFont, 20);
	// MenuItem
	private List<UserMenuItem> UserMenuItems = new ArrayList<UserMenuItem>();
	private List<SizeMenuItem> sizeMenuItems = new ArrayList<SizeMenuItem>();
	private List<FontMenuItem> fontMenuItems = new ArrayList<FontMenuItem>();
	// Combox
	private final JToolBar toolBar = new JToolBar();
	private final JCheckBox BonusCheckBox = new JCheckBox("NoBonus");
	private final JCheckBox onTopCheckBox = new JCheckBox("AlwaysOnTop");
	private final JCheckBox isResizeWin = new JCheckBox("isResizeWindows");
	private final JComboBox comboBox = new JComboBox();

	// Game Property
	private GameListFactory gameList;
	@Setter
	@Getter
	private int gameUsers = 1;
	@Setter
	@Getter
	private String url;
	@Getter
	@Setter
	private int loop = 1;
	@Getter
	@Setter
	private int waitTime = 1000;
	private SpinGame gameStart;

	public void initialize(String title, GameListFactory gameList) {
		this.gameList = gameList;
		initializePropertiesUtil();
		setLocalService(new JFrame());
		PropertiesUtil.setInitializeFrameProperty();
		initialMenuItems();
		initializeMenu();
		initializeComboBox(gameList.getGameList());
		initializeToolBar();
		initializeActionListen();
		getLocalService().setJMenuBar(menuBar);
		getLocalService().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getLocalService().setTitle(title);
		JPanel panel = (JPanel) getLocalService().getContentPane();
		panel.add(textCmd, BorderLayout.CENTER);
		LocalService.getContentPane().add(toolBar, BorderLayout.NORTH);
		// set property
		onTopCheckBox.setSelected(PropertiesUtil.getCheckBoxStatus("isontop_state"));
		isResizeWin.setSelected(PropertiesUtil.getCheckBoxStatus("resize_state"));
		if (PropertiesUtil.getProperty("ws_url") != null)
			url = PropertiesUtil.getProperty("ws_url");
		else
			url = "ws://ws-ptm-dev.sgplay.biz";
		if (PropertiesUtil.getProperty("gmae_users") != null)
			gameUsers = Integer.parseInt(PropertiesUtil.getProperty("gmae_users"));
		showGameProperty();
	}

	private void initializeComboBox(Map<String, gameType> gameList) {
		List<gameType> gameTypeList = new ArrayList<gameType>(gameList.values());
		for (gameType g : gameTypeList)
			comboBox.addItem(g.getGameCode());
	}

	private void initializeToolBar() {
		toolBar.add(comboBox);
		toolBar.add(BonusCheckBox);
		toolBar.add(btnStart);
		toolBar.add(btnStop);
		toolBar.add(btnClear);
	}

	private void initializePropertiesUtil() {
		PropertiesUtil = new PropertiesUtil();
		PropertiesUtil.setCommanFrame(this);
		PropertiesUtil.setTextCmd(textCmd);
		PropertiesUtil.setInitialeTextFont();
		PropertiesUtil.setOnTopCheckBox(onTopCheckBox);
		PropertiesUtil.setIsResizeWin(isResizeWin);
	}

	private void initialMenuItems() {
		for (int i = 10; i <= 24; i++) {
			SizeMenuItem sizeMenuItem = new SizeMenuItem(String.valueOf(i));
			sizeMenuItem.setPropertiesUtil(PropertiesUtil);
			sizeMenuItems.add(sizeMenuItem);
		}
		GraphicsEnvironment graphicsEnv = GraphicsEnvironment.getLocalGraphicsEnvironment();
		String[] listOfFontFamilyNames = graphicsEnv.getAvailableFontFamilyNames();
		for (String f : listOfFontFamilyNames) {
			FontMenuItem fontMenuItem = new FontMenuItem(f);
			fontMenuItem.setPropertiesUtil(PropertiesUtil);
			fontMenuItems.add(fontMenuItem);
		}
		fontScrollPane.setBottomFixedCount(9);
		for (int i = 10; i <= 200; i = i + 10) {
			UserMenuItem UserMenuItem = new UserMenuItem(String.valueOf(i));
			UserMenuItems.add(UserMenuItem);
		}
	}

	private void initializeMenu() {

		for (FontMenuItem f : fontMenuItems) {
			mmtmFont.add(f);
		}
		for (SizeMenuItem s : sizeMenuItems) {
			mmtmFontSize.add(s);
		}
		for (UserMenuItem u : UserMenuItems) {
			mmtmUsers.add(u);
		}
		mnSetting.add(mmtmUsers);
		mnSetting.add(mmtmFont);
		mnSetting.add(mmtmFontSize);
		menuBar.add(mnSetting);
		menuBar.add(mnWindows);
		menuBar.add(mnHelp);
		mnHelp.add(mmtmAbout);
		mnWindows.add(onTopCheckBox);
		mnWindows.add(isResizeWin);
	}

	private void initializeActionListen() {
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStart.setEnabled(false);
				textCmd.getTextA().append("Start Test Success ! Please wait 1 minutes\n ");
				gameStart = new SpinGame(gameUsers, url,
						gameList.getGameList().get(comboBox.getSelectedItem().toString()), textCmd.getTextA(),
						BonusCheckBox.isSelected(), gameList.getIsGameBer());
				new Thread(gameStart).start();
			}
		});
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gameStart.destory();
				btnStart.setEnabled(true);
			}
		});
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textCmd.getTextA().setText("");
			}
		});
		onTopCheckBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED)
					getLocalService().setAlwaysOnTop(true);
				else
					getLocalService().setAlwaysOnTop(false);
			}
		});
		isResizeWin.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED)
					getLocalService().setResizable(true);
				else
					getLocalService().setResizable(false);
			}
		});
	}

	private void showGameProperty() {
		textCmd.textA.append("WebSocket Url  = " + url + "\n");
		textCmd.textA.append("Game Users  = " + gameUsers + "\n");
		textCmd.textA.append("Wait Time  = " + waitTime / 1000 + "秒\n");
		textCmd.textA.append("Game loop  = " + loop + "\n");
	}

	protected void checkSingleInstance(int srvPort) {
		try { // 啟用ServerSocket，用以控制只能開啟一個程序
			srvSocket = new ServerSocket(srvPort);
		} catch (Exception ex) {
			if (ex.getMessage().indexOf("Address already in use: JVM_Bind") >= 0) {
				JOptionPane.showMessageDialog(getLocalService(), "該程式已啟動", "提示", JOptionPane.OK_OPTION);
				System.exit(0);
			}
		}
	}

	public JFrame getLocalService() {
		return LocalService;
	}

	public void setLocalService(JFrame localService) {
		LocalService = localService;
		LocalService.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				PropertiesUtil.SaveConfig();
			}
		});
	}

	public textArea getTextCmd() {
		return textCmd;
	}
}
