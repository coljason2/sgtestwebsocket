package com.Login;

import lombok.Data;

@Data
public class LoginResponse {

	private String sessionId;
	private acct acct;
	private String token;
	private int code;
	private String serialNo;


}
