package com.Login;

import lombok.Data;

@Data
public class setting {
	private String gameCode;
	private Double minBet;
	private Double maxBet;
	private Double maxSpotBet;
	private int line;
	private int[] lineBet;
	private double[] domination;
	private double defaultLineBet;
	private double defaultDomination;
	private int[] waysCredit;

	
}
