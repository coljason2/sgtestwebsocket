package com.Login;

import lombok.Data;

@Data
public class StartGameRequest {
	private String gameCode;
	private String serialNo;
	private String sessionId;
	private String token;
	private int records;

}
