package com.Login;

import lombok.Data;

@Data
public class StartGameResponse {

	private String gameCode;
	private setting setting;
	private scatter[] scatter;
	private int freeSpinUncompleted;
	private int freeSpinMultiply;
	private int bonusRoundUncompleted;
	private int bonusRoundNo;
	private int remainingCount;
	private int bonusRoundMultiply;
	private boolean backHome;
	private int code;
	private String serialNo;
	private Double[] gameOdd;
	private int index;
	private Double[] betOdd;
	private Double defaultBetOdd;
	private Double jpAmt;

}
