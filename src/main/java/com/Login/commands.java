package com.Login;

public interface commands {
	public static final String success = "0";
	public static final int Login = 1;
	public static final int Logout = 2;
	/**
	 * Rng Slot Game
	 */
	public static final int RngSlotStart = 400;
	public static final int RngSlotSpin = 401;
	// public static final int RngLuckySpin = 402;

	public static final int RngSlotGambleCard = 405;
	public static final int RngSlotGambleWar = 406;
	public static final int RngSlotGambleWarCard = 407;
	public static final int RngSlotGambleDice = 408;

	public static final int RngSlotBonusSpin = 410;

	public static final int RngSlotFreeGameManual = 411;

	/** slot特殊的游戏, 单独的接口( Fist of Gold ) */
	public static final int RngSlotFistOfGoldSpin = 412;

	/** slot一条线游戏接口 */
	public static final int RngSlotOneLineGameSpin = 461;

	/** slot特殊的游戏, 单独的接口 **/
	public static final int RngSlotUltimateSpin = 415;
	public static final int RngSlotUltimateDealer = 416;
	public static final int RngSlotUltimateDoubleDown = 417;

	/**
	 * Rng Table Game
	 */
	public static final int RngTableStart = 430;
	public static final int RngTableBet = 431;
	public static final int RngTableHistory = 432;

	/**
	 * Muti Player Game
	 */
	public static final int MpDerbyStart = 440;
	public static final int MpDerbyNew = 441;
	public static final int MpDerbyBet = 442;
	public static final int MpDerbyHistory = 443;

	public static final int MpWheelStart = 445;
	public static final int MpWheelNew = 446;
	public static final int MpWheelBet = 447;
	public static final int MpWheelHistory = 448;
	public static final int MpWheelResult = 449;

	/**
	 * Rng Arcade Game
	 */
	public static final int RngArcadeStart = 425;
	public static final int RngArcadeNew = 426;
	public static final int RngArcadeBet = 427;
	public static final int RngArcadeHistory = 428;
	public static final int RngArcadeBonusSpin = 429;
}
