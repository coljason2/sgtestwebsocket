package com.Login;

import java.util.UUID;

import lombok.Data;
@Data
public class LoginRequest {
	private String acctId;
	private String passwd;
	private String channel;
	private String serialNo;

	public LoginRequest() {
		this.passwd = "000000";
		this.serialNo = UUID.randomUUID().toString();
		this.channel = "mobile";
	}

}
