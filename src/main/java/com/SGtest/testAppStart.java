package com.SGtest;

import java.awt.EventQueue;
import com.frame.CommanFrame;
import com.frame.ShowErrorMessageFrame;
import com.game.GameListFactory;


public class testAppStart extends CommanFrame {
	GameListFactory game = new GameListFactory();

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					testAppStart window = new testAppStart();
					// window.checkSingleInstance(5566);
					window.getLocalService().setVisible(true);
				} catch (Exception e) {
					new ShowErrorMessageFrame(e);
				}
			}
		});
	}

	public testAppStart() {
		//System.out.println(game.getGameList());
		initialize("SpadeGaming Test Plan", game);
	}
}
