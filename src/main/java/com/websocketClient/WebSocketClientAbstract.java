package com.websocketClient;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.util.Random;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.swing.JTextArea;
import javax.websocket.ClientEndpoint;
import javax.websocket.ContainerProvider;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import com.Login.LoginRequest;
import com.Login.LoginResponse;
import com.Login.StartGameRequest;
import com.Login.StartGameResponse;
import com.Login.commands;
import com.SpinRequest.Spin401Request;
import com.SpinRequest.Spin406Request;
import com.SpinRequest.Spin407Request;
import com.SpinRequest.Spin411Request;
import com.SpinRequest.Spin412Request;
import com.SpinRequest.Spin427Request;
import com.SpinRequest.SpinRequest;
import com.SpinRequest.freeGame;
import com.alibaba.fastjson.JSON;
import com.game.gameType;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ClientEndpoint
public abstract class WebSocketClientAbstract implements Runnable {
	public Session userSession = null;
	// test properity
	protected LoginRequest loginRequest = new LoginRequest();
	protected LoginResponse loginResponse = new LoginResponse();
	protected StartGameRequest startGameRequest;
	protected StartGameResponse startgameResponse;
	protected SpinRequest spinRequest;
	protected gameType gameType;
	protected JsonObject response = null;
	public String code;
	public int getCommandCode;
	public double Get401totalwin;
	@Setter
	@Getter
	public boolean noBonus = true;
	@Setter
	public JTextArea area;
	@Setter
	private Set<String> IsGameBer;

	public WebSocketClientAbstract(String endpointURI, gameType game) {
		try {
			WebSocketContainer container = ContainerProvider.getWebSocketContainer();
			container.connectToServer(this, new URI(endpointURI));
			this.gameType = game;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@OnMessage
	public void onMessage(String message) throws InterruptedException {
		log.info(message);
	}

	/**
	 * Send a message.
	 * 
	 * @param user
	 * @param message
	 */
	public void sendMessage(int commandCode, Object message) {
		// log.info("sendMessage : {}", commandCode + "." +
		// JSON.toJSONString(message));
		this.userSession.getAsyncRemote().sendText(commandCode + "." + JSON.toJSONString(message));
	}

	public void initLogin() {
		// loginRequest.setAcctId("TESTPLAYER100@TEST");
		// log.info("initLogin {}", JSON.toJSONString(loginRequest));
		sendMessage(commands.Login, loginRequest);
	}

	public void checkSpinType(String message) {
		if (startgameResponse == null)
			startgameResponse = JSON.parseObject(message.substring(message.indexOf("{")), StartGameResponse.class);
		switch (gameType.getSpinType()) {
		case commands.RngSlotSpin:
			BuildSend401Message(commands.RngSlotSpin);
			break;
		case commands.RngSlotFistOfGoldSpin:
			BuildSend412Message();
			break;
		case commands.RngArcadeBet:
			BuildSend427Message();
			break;
		case commands.RngSlotOneLineGameSpin:
			BuildSend401Message(commands.RngSlotOneLineGameSpin);
			break;
		default:
			log.info("No Message Build ");
		}

	}

	public void initStartGameRequest(String message) throws InterruptedException {
		loginResponse = JSON.parseObject(message.substring(message.indexOf("{")), LoginResponse.class);
		startGameRequest = new StartGameRequest();
		startGameRequest.setGameCode(gameType.getGameCode());
		startGameRequest.setSerialNo(loginResponse.getSerialNo());
		startGameRequest.setSessionId(loginResponse.getSessionId());
		startGameRequest.setToken(loginResponse.getToken());
		// log.info("initStartGameRequest : {}", commands.RngSlotStart + "." +
		// JSON.toJSONString(startGameRequest));
		if (area != null)
			area.append(JSON.toJSONString(loginResponse) + "\n");

		switch (gameType.getGameType()) {
		case commands.RngSlotStart:
			sendMessage(commands.RngSlotStart, startGameRequest);
			break;
		case commands.RngArcadeStart:
			sendMessage(commands.RngArcadeStart, startGameRequest);
			sendMessage(commands.RngArcadeNew, startGameRequest);
			startGameRequest.setRecords(50);
			sendMessage(commands.RngArcadeHistory, startGameRequest);
			break;
		default:
			log.info("No Match Game Type Number ");
		}

	}

	public void BuildSend401Message(int command) {
		Spin401Request spin401Request = new Spin401Request();
		spin401Request.setUnit(startgameResponse.getSetting().getDefaultDomination());
		spin401Request.setLine(startgameResponse.getSetting().getLine());
		spin401Request.setDomination(startgameResponse.getSetting().getDefaultDomination());
		spin401Request.setGameCode(gameType.getGameCode());
		spin401Request.setTotalBet(gameType.getTotalBet());
		spin401Request.setSerialNo(loginResponse.getSerialNo());
		spin401Request.setSessionId(loginResponse.getSessionId());
		spin401Request.setToken(loginResponse.getToken());
		if (startgameResponse.getSetting().getWaysCredit().length != 0)
			spin401Request.setCredit(startgameResponse.getSetting().getWaysCredit()[0]);
		spin401Request.setRetainResult(null);
		if (response.get("freeGame") != null) {
			freeGame freeGame = JSON.parseObject(response.get("freeGame").toString(), freeGame.class);
			// log.info("freeGame = {} ", freeGame.toString());
			spin401Request.setFreeSpin(freeGame.getSpins());
		} else {
			spin401Request.setFreeSpin(1);
		}
		spinRequest = spin401Request;
		if (response.get("bonusGame") != null) {
			BuildSend410Message();
		} else if (response.toString().indexOf("hits") > -1 && IsGameBer.contains(gameType.getGameCode())) {
			Get401totalwin = Double.parseDouble(response.get("totalWin").toString());
			BuildSend407Message();
		} else if (response.get("roundId") != null && Integer.parseInt(response.get("roundId").toString()) > 0
				&& response.get("freeGame") == null) {
			BuildSend411Message();
		} else {
			sendMessage(command, spinRequest);
		}

	}

	// bounsGame
	public void BuildSend410Message() {
		spinRequest = new SpinRequest();
		// game_totalBet
		spinRequest.setTotalBet(gameType.getTotalBet());
		spinRequest.setGameCode(gameType.getGameCode());
		spinRequest.setSerialNo(loginResponse.getSerialNo());
		spinRequest.setSessionId(loginResponse.getSessionId());
		spinRequest.setToken(loginResponse.getToken());
		log.info("bonusGame : {} ", commands.RngSlotBonusSpin + "." + JSON.toJSONString(spinRequest));
		sendMessage(commands.RngSlotBonusSpin, spinRequest);
	}

	public void BuildSend411Message() {
		Spin411Request Spin411Request = new Spin411Request();
		int SpingIndex = (int) (Math.random() * 4);
		int freeSping = 6;
		Spin411Request.setFreeSpinIndex(SpingIndex);
		switch (SpingIndex) {
		case 0:
			freeSping = 25;
			break;
		case 1:
			freeSping = 15;
			break;
		case 2:
			freeSping = 10;
			break;
		case 3:
			freeSping = 6;
			break;
		}
		Spin411Request.setFreeSpins(freeSping);
		Spin411Request.setGameCode(gameType.getGameCode());
		Spin411Request.setSerialNo(loginResponse.getSerialNo());
		Spin411Request.setSessionId(loginResponse.getSessionId());
		Spin411Request.setToken(loginResponse.getToken());
		spinRequest = Spin411Request;
		sendMessage(commands.RngSlotFreeGameManual, spinRequest);
	}

	// RngSlotGambleWarCard
	public void BuildSend407Message() {
		// log.info("BuildSend407Message");
		Spin407Request spinRequest = new Spin407Request();
		// game_totalBet
		// spinRequest.setTotalBet(null);
		spinRequest.setGameCode(gameType.getGameCode());
		spinRequest.setSerialNo(loginResponse.getSerialNo());
		spinRequest.setSessionId(loginResponse.getSessionId());
		spinRequest.setToken(loginResponse.getToken());
		// log.info("GambleWarCard : {} ", commands.RngSlotGambleWarCard + "." +
		// JSON.toJSONString(spinRequest));
		sendMessage(commands.RngSlotGambleWarCard, spinRequest);
	}

	public void BuildSend406Message() {
		Spin406Request spinRequest = new Spin406Request();
		spinRequest.setGameCode(gameType.getGameCode());
		spinRequest.setSerialNo(loginResponse.getSerialNo());
		spinRequest.setSessionId(loginResponse.getSessionId());
		spinRequest.setToken(loginResponse.getToken());
		spinRequest.setBankerCardId(Integer.parseInt(response.get("cardId").toString()));
		spinRequest.setDomination(startgameResponse.getSetting().getDefaultDomination());
		spinRequest.setTotalBet(Get401totalwin);
		// 1:small 2:big
		spinRequest.setGambleId(new Random().nextInt(2) + 1);
		// log.info(commands.RngSlotGambleWar + "." +
		// JSON.toJSONString(spinRequest));
		sendMessage(commands.RngSlotGambleWar, spinRequest);
	}

	public void BuildSend412Message() {
		Spin412Request spin412Request = new Spin412Request();
		spin412Request.setUnit(0.02);
		spin412Request.setLine(startgameResponse.getSetting().getLine());
		if (startgameResponse.getSetting().getWaysCredit().length != 0)
			spin412Request.setCredit(startgameResponse.getSetting().getWaysCredit()[0]);
		spin412Request.setDomination(0.02);
		spin412Request.setGameCode(gameType.getGameCode());
		spin412Request.setTotalBet(gameType.getTotalBet());
		if (response.get("freeGame") != null) {
			freeGame freeGame = JSON.parseObject(response.get("freeGame").toString(), freeGame.class);
			// log.info("freeGame = {} ", freeGame.toString());
			spin412Request.setFreeSpin(freeGame.getSpins());
		} else {
			spin412Request.setFreeSpin(1);
		}
		spin412Request.setSerialNo(loginResponse.getSerialNo());
		spin412Request.setSessionId(loginResponse.getSessionId());
		spin412Request.setToken(loginResponse.getToken());
		spinRequest = spin412Request;
		// log.info(commands.RngSlotFistOfGoldSpin + "." +
		// JSON.toJSONString(spinRequest));
		if (response.get("bonusGame") != null) {
			BuildSend410Message();
		} else {
			sendMessage(commands.RngSlotFistOfGoldSpin, spinRequest);
		}
	}

	public void BuildSend427Message() {
		Random rngNo = new Random();
		int totalBet = 0;
		Spin427Request Spin427Request = new Spin427Request();
		Spin427Request.setDomination(1);
		Spin427Request.setGameCode(gameType.getGameCode());
		Spin427Request.setIndex(0);
		Spin427Request.setSerialNo(loginResponse.getSerialNo());
		Spin427Request.setToken(loginResponse.getToken());
		Spin427Request.setSessionId(loginResponse.getSessionId());
		int[] locBet = { rngNo.nextInt(5), rngNo.nextInt(5), rngNo.nextInt(5), rngNo.nextInt(5), rngNo.nextInt(5),
				rngNo.nextInt(5), rngNo.nextInt(5), rngNo.nextInt(5), rngNo.nextInt(5) };
		Spin427Request.setLocBet(locBet);
		for (int i : locBet) {
			totalBet = totalBet + i;
		}
		Spin427Request.setTotalBet(totalBet);
		if (response.get("bonusRound") != null && Integer.parseInt(response.get("bonusRound").toString()) > 0) {
			BuildSend429Message(spinRequest);
		} else if (response.get("bonus") != null && Integer.parseInt(response.get("bonus").toString()) > 0) {
			BuildSend429Message(spinRequest);
		} else {
			spinRequest = Spin427Request;
			sendMessage(commands.RngArcadeBet, spinRequest);
		}
	}

	public void BuildSend429Message(SpinRequest spinRequest) {
		Spin427Request OriginalSpin = (Spin427Request) spinRequest;
		Spin427Request Spin427Request = new Spin427Request();
		Spin427Request.setDomination(1);
		Spin427Request.setGameCode(gameType.getGameCode());
		Spin427Request.setTotalBet(OriginalSpin.getTotalBet());
		Spin427Request.setIndex(0);
		Spin427Request.setSerialNo(loginResponse.getSerialNo());
		Spin427Request.setToken(loginResponse.getToken());
		Spin427Request.setSessionId(loginResponse.getSessionId());
		Spin427Request.setLocBet(OriginalSpin.getLocBet());
		spinRequest = Spin427Request;
		sendMessage(commands.RngArcadeBonusSpin, spinRequest);
	}

	public void parseResponseMessage(String message) {
		JsonReader reader = Json.createReader(new StringReader(message.substring(message.indexOf("{"))));
		response = reader.readObject();
		reader.close();
	}

	public void destory() {
		// log.info("Destory session {} {}", userSession.getId(),
		// loginResponse.getAcct().getAcctId());
		if (userSession != null) {
			try {
				userSession.close();
				userSession = null;
			} catch (IOException e) {
				userSession = null;
			}
		}
	}
}
