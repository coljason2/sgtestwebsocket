package com.websocketClient;

import java.util.Date;
import javax.websocket.ClientEndpoint;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import lombok.extern.slf4j.Slf4j;
import com.Login.commands;
import com.game.gameType;

@Slf4j
@ClientEndpoint
public class websocketClientFrame extends WebSocketClientAbstract {

	public websocketClientFrame(String endpointURI, gameType game) {
		super(endpointURI, game);
	}

	@OnOpen
	public void onOpen(Session userSession) {
		this.userSession = userSession;
		// log.info("open : {}", userSession);
	}

	@OnError
	public void onError(Session session, Throwable error) {
		log.debug("error ", loginResponse.getAcct().getAcctId());
		error.printStackTrace();
	}

	@OnMessage
	public void getResponse(Session session, String message) throws InterruptedException {
		Thread.sleep(650);
		this.userSession = session;
		parseResponseMessage(message);
		log.info("get : {}", new Object[] { message });
		code = response.get("code").toString();
		getCommandCode = Integer.parseInt(message.substring(0, message.indexOf(".")));
		if (message != null && code.equals(commands.success)) {
			switch (getCommandCode) {
			case commands.Login:
				initStartGameRequest(message);
				break;
			case commands.RngSlotStart:
				checkSpinType(message);
				break;
			case commands.RngSlotSpin:
				BuildSend401Message(commands.RngSlotSpin);
				break;
			case commands.RngSlotFistOfGoldSpin:
				BuildSend412Message();
				break;
			case commands.RngSlotBonusSpin:
				checkSpinType(message);
				break;
			case commands.RngSlotGambleWarCard:
				if (noBonus) {
					checkSpinType(message);
				} else {
					BuildSend406Message();
				}
				break;
			case commands.RngSlotGambleWar:
				checkSpinType(message);
				break;
			case commands.RngArcadeStart:
				checkSpinType(message);
				break;
			case commands.RngArcadeBet:
				BuildSend427Message();
				break;
			case commands.RngArcadeBonusSpin:
				checkSpinType(message);
				break;
			case commands.RngSlotFreeGameManual:
				BuildSend401Message(commands.RngSlotSpin);
				break;
			case commands.RngSlotOneLineGameSpin:
				BuildSend401Message(commands.RngSlotOneLineGameSpin);
				break;
			default:
				userSession = null;
				// log.info("no commands close client");
				break;
			}
		} else {
			destory();
		}
	}

	public void run() {
		/**
		 * Login
		 */
		String message;
		String balance = null;
		initLogin();
		while (userSession != null) {
			try {
				Thread.sleep(59000);
				if (response.get("balance") != null)
					balance = response.get("balance").toString();
				message = "Game :" + gameType.getGameCode() + "/ user :" + loginResponse.getAcct().getAcctId()
						+ "/ balance :" + balance + " /time :" + new Date().toString();
				// log.info("user : {}/balance : {}/time : {}", new Object[] {
				// loginResponse.getAcct().getAcctId(),
				// response.get("balance").toString(), new Date().toString() });
				area.append(message + "\n");
			} catch (Exception e) {
				e.printStackTrace();
				this.userSession = null;
			}
		}
		// log.info("{} is stop because {}", new Object[] {
		// loginResponse.getAcct().getAcctId(), code });
		message = loginResponse.getAcct().getAcctId() + " stop because " + code;
		area.append(message + "\n");
	}

}
