package com.game;

public interface GameCode {
	public static final String FirstLove = "S-FL02";
	public static final String FistofGold = "S-FG01";
	public static final String ShangHai008 = "S-SH01";
	public static final String LionHeartSA = "S-LH03";
	public static final String MingDynasty = "S-DM01";
	public static final String Tarzan = "S-TZ01";
	public static final String RobStars = "A-RB01";
	public static final String FaFaFa2 = "S-LY02";
	public static final String WowProsperity = "S-WP02";
	public static final String PrincessWang = "S-PW02";
	public static final String GangsterAxe = "S-GA01";
}
