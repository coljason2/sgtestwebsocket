package com.game;

import lombok.Data;

@Data
public class gameType {
	private String gameCode;
	private int spinType;
	private double totalBet;
	private int gameType;
}
