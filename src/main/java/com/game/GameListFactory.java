package com.game;

import java.io.FileReader;
import java.io.Reader;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import com.frame.ShowErrorMessageFrame;
import com.google.common.collect.ImmutableMap;
import lombok.Getter;

/**
 *
 *
 * load gameList csv file
 *
 *
 **/
public class GameListFactory {

	private ImmutableMap.Builder<String, gameType> builder = ImmutableMap.builder();
	@Getter
	private Map<String, gameType> gameList;
	@Getter
	private Set<String> IsGameBer = new HashSet<String>();

	public GameListFactory() {

		Reader in;
		try {
			in = new FileReader("gameList.csv");
			Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().parse(in);
			for (CSVRecord r : records) {
				gameType g = new gameType();
				g.setGameCode(r.get("gameCode"));
				g.setSpinType(Integer.parseInt(r.get("spinType")));
				g.setTotalBet(Double.parseDouble(r.get("totalBet")));
				g.setGameType(Integer.parseInt(r.get("gameType")));
				builder.put(r.get("gameCode"), g);
				if (r.get("IsGameBer").equals("Y"))
					IsGameBer.add(r.get("gameCode"));
			}
			gameList = builder.build();
		} catch (Exception e) {
			new ShowErrorMessageFrame(e);
		}
	}

	// public static void main(String[] args) {
	// GameListFactory gameListFactory = new GameListFactory();
	//
	// }
}
